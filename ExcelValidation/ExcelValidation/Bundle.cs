﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelValidation
{
    class Bundle
    {
        string name;
        string id;
        List<Bundle> compatibleWith = null;

        public Bundle(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        public Bundle(string name, string id, List<Bundle> compatibleWith)
        {
            this.name = name;
            this.id = id;
            this.compatibleWith = compatibleWith;
        }
        
        public string Name { get { return name; } }
        public string Id { get { return id; } }
        public List<Bundle> Compatibility { get { return compatibleWith; } }

    }
}
