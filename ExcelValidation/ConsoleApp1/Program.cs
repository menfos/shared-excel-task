﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelValidation;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelValidator validator = new ExcelValidator(@"C:\Users\menfo\Documents\Visual Studio 2017\Projects\Excel Task\test.xlsx", 2);

            ItemsDataLocation HorizontalBundleLocation = new ItemsDataLocation("B3", "B68");
            ItemsDataLocation VerticalBundleLocation = new ItemsDataLocation("O2", "CB2");
            ItemsDataLocation compatibilityMatrixLocation = new ItemsDataLocation("O3", "CB68");
            ItemsDataLocation productIdLocation = new ItemsDataLocation("CD3", "CD68");
            //validator.BundlesIdJuxtapose(HorizontalBundleLocation,productIdLocation);
            //validator.s();
            validator.GetBundleCompatibility(HorizontalBundleLocation,VerticalBundleLocation,compatibilityMatrixLocation);

            //validator.s();
            Console.ReadLine();
        }
    }
}
