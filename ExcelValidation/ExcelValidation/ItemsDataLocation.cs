﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelValidation
{
    public class ItemsDataLocation
    {
        string startLocation;
        string endLocation;

        public ItemsDataLocation(string startLocation, string endLocation)
        {
            this.startLocation = startLocation;
            this.endLocation = endLocation;
        }

        public string StartLocation { get { return startLocation; } }
        public string EndLocation { get { return endLocation; } }
    }
}
