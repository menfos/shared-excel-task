﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;
using Wintellect.PowerCollections;

namespace ExcelValidation
{
    public class ExcelValidator
    {
        Excel.Application xlApp;
        Excel.Workbook xlWorkbook;
        Excel.Worksheet xlWorksheet;

        List<Bundle> bundles = null;

        public ExcelValidator(string path, int workBookSheet)
        {
            try
            {
                xlApp = new Excel.Application();
                xlWorkbook = xlApp.Workbooks.Open(path);
                xlWorksheet = xlWorkbook.Sheets[workBookSheet];
                
                bundles = new List<Bundle>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void BundlesIdJuxtapose(ItemsDataLocation bundleLocation, ItemsDataLocation productIdLocation)
        {
            Excel.Range bundleRange = xlWorksheet.get_Range(bundleLocation.StartLocation, bundleLocation.EndLocation);
            Excel.Range productIdRange = xlWorksheet.get_Range(productIdLocation.StartLocation, productIdLocation.EndLocation);

            for (int row = 1; row < bundleRange.Rows.Count; row++)
            {
                string bundleName = "";
                string productId = "";
                
                if (bundleRange.Cells[row, 1] != null && bundleRange.Cells[row, 1].Value2 != null )
                {
                     bundleName = bundleRange.Cells[row, 1].Value2.ToString();
                }
                else
                {
                    Console.WriteLine("No Value For Bundle. Location: {0} {1}", row, bundleRange.get_AddressLocal().Split('$')[1]);
                }

                if(productIdRange.Cells[row, 1] != null && productIdRange.Cells[row, 1].Value2 != null)
                {
                    productId = productIdRange.Cells[row, 1].Value2.ToString();
                }
                else
                {
                    Console.WriteLine("No Value For ProductId. Location: {0} {1}", row, productIdRange.get_AddressLocal().Split('$')[1]);
                }

                bundles.Add(new Bundle(bundleName, productId));
            }
            foreach(var tuple in bundles)
            {
                Console.WriteLine("KEY {0} VALUES: {1} ", tuple.Id, tuple.Name);
            }
        }


        public void GetBundleCompatibility(ItemsDataLocation horizontalBandle, ItemsDataLocation verticalBandle, ItemsDataLocation compatibilityMatrix)
        {
            Excel.Range horizontalBandleRange = xlWorksheet.get_Range(horizontalBandle.StartLocation, horizontalBandle.EndLocation);
            Excel.Range verticalBandleRange = xlWorksheet.get_Range(verticalBandle.StartLocation, verticalBandle.EndLocation);
            Excel.Range compatibilityMatrixRange = xlWorksheet.get_Range(compatibilityMatrix.StartLocation, compatibilityMatrix.EndLocation);

            for(int row = 1; row <= compatibilityMatrixRange.Rows.Count; row++)
            {
                Console.WriteLine((compatibilityMatrixRange[row, 1] as Excel.Range).Interior.ColorIndex);
            }
        }
        
    }
}
